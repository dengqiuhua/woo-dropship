//动态加载插件
function reload() {
    try {
        console.log("exec reload");
        chrome.runtime.reload && chrome.runtime.reload();
    } catch(e) {
        console.log("reload fail");
    }
}

//消息监听
function onMessage(request, sender, sendResponse) {
    console.log("=====received:", request)
    if (request.event === 'reload') {
        reload();
        sendResponse && sendResponse("reload finished");
    } else if (request.event === 'getConfig') {
        if(!chrome.storage.local) {
            sendResponse && sendResponse(null);
            return;
        }
        //数据来源:popup
        chrome.storage.local.get(["woo-cfg"], function(result) {
            let cfg = result && result["woo-cfg"] ? JSON.parse(result["woo-cfg"]) : null;
            sendResponse && sendResponse(cfg);
            return true;
        });
    }
    //保持消息通道打开直到异步响应返回
    return true;
}


chrome.runtime && chrome.runtime.onMessage.addListener(onMessage);


/*
chrome.runtime.onInstalled.addListener(function() {
    chrome.storage.local.get(['woo-key'], function(result){
        console.log('Local Woo-Key is', result['woo-key']);
    });
});
*/

//修改headers规则
function modifyHeaders() {
    try {
        if(!chrome.runtime.onInstalled || !chrome.declarativeNetRequest) {
            return
        }

        chrome.runtime.onInstalled.addListener((function() {
            var js = {
                id: 1,
                condition: {
                    urlFilter: "|*",
                    resourceTypes: ["main_frame", "sub_frame"]
                },
                action: {
                    type: "modifyHeaders",
                    responseHeaders: [{
                        header: "x-frame-options",
                        operation: "remove"
                    },
                    {
                        header: "content-security-policy",
                        operation: "remove"
                    }]
                }
            },
            cross = {
                id: 2,
                condition: {
                    urlFilter: "*",
                    resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
                },
                action: {
                    type: "modifyHeaders",
                    requestHeaders: [{
                        header: "Access-Control-Allow-Origin",
                        operation: "set",
                        value: "*"
                    }],
                    responseHeaders: [{
                        header: "Access-Control-Allow-Origin",
                        operation: "set",
                        value: "*"
                    }]
                }
            };
            chrome.declarativeNetRequest.updateDynamicRules({
                removeRuleIds: [js.id],
                addRules: [js]
            }),
            chrome.declarativeNetRequest.updateDynamicRules({
                removeRuleIds: [cross.id],
                addRules: [cross]
            })
        }))

    } catch (e) {
        console.log("[headers] modify fail");
    }
}

modifyHeaders();
