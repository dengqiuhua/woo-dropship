// (function() {

var Options = function() {

}

Options.prototype.saveContent = function(content) {
    Storages.save({test: content});
}

Options.prototype.loadContent = function() {
    Storages.get(["test"], function(res) {
        console.log("content:", res);
    });
}

function onPageLoad() {
    var opt = new Options();
    $("button[name=btn-save]").on('click', saveContent);
    $("button[name=btn-get]").on('click', opt.loadContent);
}

function saveContent(e) {
    var content = $("textarea[name=content]").val().trim();
    if(!content) {
        $("textarea[name=content]").focus();
        return false;
    }
    var opt = new Options();
    opt.saveContent(content);
}

onPageLoad();

// })();

