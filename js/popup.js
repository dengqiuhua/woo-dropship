(function() {
    var Popup = function() {
        this.cacheKeyName = "woo-cfg";
        this.defaultHost = "https://drop.lazywork.cn";
    }

    Popup.prototype.notify = function(msg) {
        Message && Message.send(msg, function(resp) {
            console.log("resp:", resp);
        });
        Message && Message.sendTab(msg, function(resp) {
            console.log("resp:", resp);
        });
    }

    Popup.prototype.loadConfig = function(callback) {
        var _this = this;
        Storages.get([this.cacheKeyName], function(res) {
            if(res && res[_this.cacheKeyName]) {
                let cfg = res[_this.cacheKeyName] ? JSON.parse(res[_this.cacheKeyName]) : null;
                callback && callback(cfg);
            } else {
                callback && callback(null);;
            }
        });
    }

    Popup.prototype.onLoad = function() {
        var _this = this;
        this.loadConfig(function(cfg) {
            if(cfg) {
                _this.fillValue(cfg);
                _this.onToggleForm(false);
            } else {
                $("input[name=host]").val(_this.defaultHost);
                $(".default-site").text(_this.defaultHost);
                _this.onToggleForm(true);
            }
        });
    }

    Popup.prototype.setConfig = function(cfg) {
        var mapping = {},
            _this = this;
        mapping[this.cacheKeyName] = cfg && typeof cfg === 'object' ? JSON.stringify(cfg) : cfg;
        Storages.save(mapping, function() {
            if(cfg) {
                _this.fillValue(cfg);
                _this.onToggleForm(false);
            }
            console.log("set finished");
        });
    }

    Popup.prototype.fillValue = function(cfg) {
        if(!cfg || typeof cfg !== 'object') {
            cfg = {key: "", host: ""};
        }
        $("#span-key").html(cfg.key);
        $("#span-host").html(cfg.host);
    }

    Popup.prototype.onToggleForm = function(isFormShow) {
        if(isFormShow) {
            $("div.key-box").addClass("hide");
            $("form.form").removeClass("hide");
        } else {
            $("div.key-box").removeClass("hide");
            $("form.form").addClass("hide");
        }
    }

    Popup.prototype.onEdit = function() {
        $("#btn-cancel").removeClass("hide");
        var pop = new Popup();
        pop.loadConfig(function(cfg) {
            if(cfg) {
                $(".default-site").text(cfg.host);
                $("input[name=host]").val(cfg.host);
            }
            pop.onToggleForm(true);
        });

    }

    Popup.prototype.onCancel = function() {
        new Popup().onToggleForm(false);
    }

    Popup.prototype.onChangeHost = function() {
        $(".default-site").addClass("hide");
        $("#btn-site").addClass("hide");
        $("input[name=host]").removeClass("hide");
    }

    Popup.prototype.onRemove = function() {
        var pop = new Popup();
        pop.setConfig(null);
        pop.fillValue(null);
        setTimeout(() => {
            pop.onLoad();
            // pop.onToggleForm(true);
        }, 1200);
        var msg = {event: "deletekey", runtime_id: chrome.runtime.id};
        pop.notify(msg);
    }

    Popup.prototype.onKeySubmit = function() {
        var key = $("input[name=key]").val().replace(/ /g, ''),
            host = $("input[name=host]").length > 0 ? $("input[name=host]").val().replace(/ /g, '') : "";
        if(!key) {
            $("input[name=key]").focus();
            return;
        }
        if(host) {
            if(!host.startsWith('http')) {
                host = 'http://'+ host;
            }
            //域名合法性校验
            const urlRegex = /^(http(s)?:\/\/)\w+[^\s]+(\.[^\s]+){1,}$/;
            if(!urlRegex.test(host)) {
                $("input[name=host]").focus();
                return;
            }

        } else {
            host = this.defaultHost;
        }
        //存储到本地
        var pop = new Popup();
        var cfg = {key: key, host: host};
        pop.setConfig(cfg);
        $("input[name=key]").val("");
        $("input[name=host]").val("");
        //通知信息
        // var msg = {event: "set_config", key: key, runtime_id: chrome.runtime.id};
        // pop.notify(msg);

        //远程调用
        // var url = "http://127.0.0.1:8000?from=popup&r=" + Math.random();
        // $.get(url, function(msg) {
        //     console.log("req resp:", msg);
        // })
    }

    Popup.prototype.onMessage = function(message, sender, sendResponse) {
        console.log("receiv msg:", message);
        sendResponse & sendResponse({msg: "知道啦"});
    }

    function onPageLoad() {
        var pop = new Popup();
        pop.onLoad();

        //接送消息
        Message.accept(pop.onMessage)
        //保存key
        $("button[name=btn-submit]").on('click', pop.onKeySubmit);
        //修改
        $("#btn-edit").on('click', pop.onEdit);
        //取消修改
        $("#btn-cancel").on('click', pop.onCancel);
        //删除
        $("#btn-remove").on('click', pop.onRemove);
        //更换站点
        $("#btn-site").on('click', pop.onChangeHost);
    }

    onPageLoad();
})();