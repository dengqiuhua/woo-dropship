var Storages = {
    isAvailable() {
        return typeof chrome.storage !== 'undefined';
    },

    //保存数据
    save(mappings, callback) {
        if(!this.isAvailable()) {
            callback && callback(false, "`chrome.storage` not availble.");
            return false;
        }
        if(typeof mappings !== 'object') {
            callback && callback(false, "`mappings` not a object.");
            return false;
        }
        chrome.storage.local.set(mappings, function(){
            console.log("saved");
            callback && callback(true);
		});
    },
    //获取数据
    get(keys, callback) {
        if(!this.isAvailable()) {
            callback && callback(false, "`chrome.storage` not availble.");
            return false;
        }
        if(typeof keys !== 'object') {
            callback && callback(false, "`keys` not an array.");
            return false;
        }
        chrome.storage.local.get(keys, function(res){
            callback && callback(res);
		});
    },
    //监听
    watch(keys, callback) {
        if(!this.isAvailable()) {
            callback && callback(false, "`chrome.storage` not availble.");
            return false;
        }
        if(typeof keys !== 'object') {
            callback && callback(false, "`keys` not an array.");
            return false;
        }
        chrome.storage.onChanged.addListener(keys, function(res, namespace){
            console.log("changed");
            callback && callback(res, namespace);
		});
    }
}


var Message = {
    //给content-script发送消息
    sendTab(message, callback) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, message, function(response) {
                callback && callback(response);
            });
        });
    },
    //给后台发消息
    send(message, callback) {
        chrome.runtime.sendMessage(message);
    },
    accept(callback) {
        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            console.log('收到来自content-script的消息：');
            console.log(request, sender, sendResponse);
            callback && callback(request, sender, sendResponse);
            // sendResponse('我是后台，我已收到你的消息：' + JSON.stringify(request));

        });
    }
}
