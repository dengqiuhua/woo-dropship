const {randomString} = require("./helper");

//Google热词采集
var Google = function() {
};

//消息提示
Google.$msgbox = null;

//监听事件
Google.prototype.onload = function($msgbox) {
    var style = 'position: fixed;\
        width: 360px;\
        right: 0px;\
        z-index: 999;\
        height: 100%;\
        display: block;\
        top: 0;\
        background-color: #cc6;\
        padding: 5px;\
        bottom: 42px;\
        overflow: scroll;';

    var style_ext = 'margin-top: 6px; border-top: 1px dashed #eee; padding-top: 5px;'

    var html = '<div id="kw-drawer" style="'+ style +'">\
        <ul id="kw-related"></ul>\
        <ul id="kw-extend" style="'+ style_ext +'"></ul>\
        </div>';
    $("body").append(html);
    //关联词
    this.relatedWords();
    this.relatedFaq();

    //操作
    var opt = '点击更多相关搜索, <a id="kw-copy" href="javascript:;">复制</a>\
     | <a id="kw-show" href="javascript:;">隐藏</a>';
    $msgbox.html(opt);
    Google.$msgbox = $msgbox;

    $("#kw-copy").on("click", this.copy);
    $("#kw-show").on("click", this.toggleExtend);
}

//自行拼凑关键词寻找关联词
Google.prototype.suggest = function() {
    const inputField = document.getElementById('APjFqb');
    if(!inputField || !inputField.value) {
        return;
    }
    const originalValue = inputField.value.toLowerCase();  // 保存原始值
    if(!originalValue || originalValue.length < 2) {
        Google.$msgbox = "请输入搜索关键词";
        return;
    }
    const chars = 'wosaibcdefghjklmnpqrtuvxyz'.split('');
    $("#kw-extend").html("");

    const _this = this;
    this.query(originalValue, originalValue, function(flag, res) {
        if(flag && res) {
            _this.append(_this.formated(originalValue, res));
        }
    });

    for(let c of chars) {
        this.query(originalValue, originalValue + " "+ c, function(flag, res) {
            if(flag && res) {
                _this.append(_this.formated(originalValue, res));
            }
        });
    }

    this.show();
}

Google.prototype.query = function(basic_kw, keyword, callback) {
    //https://www.google.com/complete/search?q=bottle&cp=0&client=desktop-gws-wiz-on-focus-serp&xssi=t&gs_pcrt=undefined&hl=en-HK&authuser=0&pq=bottle&psi=_-s1ZvTjOaDj2roPk6ut6AE.1714809857006&dpr=1&ofp=EAEylwEKDgoMd2F0ZXIgYm90dGxlChEKD3dhdGVyIGJvdHRsZSBoawoWChRib3R0bGUgcHJvbnVuY2lhdGlvbgoQCg5ib3R0bGUgY2hpbmVzZQoOCgxmbGFzayBib3R0bGUKEAoOcGxhc3RpYyBib3R0bGUKDwoNYm90dGxlIHB5dGhvbgoTChF3YXRlciBib3R0bGUgbXVqaRBHMnUKJgokV2hhdCBkbyB3ZSBjYWxsIGEgYm90dGxlIGluIEVuZ2xpc2g_Ch0KG1doYXQgZG8geW91IG1lYW4gYnkgYm90dGxlPwopCidXaGF0IGRvZXMgdGhlIEJyaXRpc2ggd29yZCBib3R0bGUgbWVhbj8Q5AI
    //https://www.google.com/complete/search?q=bottle&cp=0&client=desktop-gws-wiz-on-focus-serp&xssi=t&gs_pcrt=3&hl=en-HK&authuser=0&pq=bottle&psi=fO81ZpzdN-jT2roPqv2OqA8.1714810751326&dpr=1&ofp=EAEylwEKDgoMd2F0ZXIgYm90dGxlChEKD3dhdGVyIGJvdHRsZSBoawoWChRib3R0bGUgcHJvbnVuY2lhdGlvbgoQCg5ib3R0bGUgY2hpbmVzZQoOCgxmbGFzayBib3R0bGUKEAoOcGxhc3RpYyBib3R0bGUKDwoNYm90dGxlIHB5dGhvbgoTChF3YXRlciBib3R0bGUgbXVqaRBHMnUKJgokV2hhdCBkbyB3ZSBjYWxsIGEgYm90dGxlIGluIEVuZ2xpc2g_Ch0KG1doYXQgZG8geW91IG1lYW4gYnkgYm90dGxlPwopCidXaGF0IGRvZXMgdGhlIEJyaXRpc2ggd29yZCBib3R0bGUgbWVhbj8Q5AI
    let url = 'https://'+ document.location.hostname +'/complete/search';
    let params = {
        q: keyword,
        cp: keyword.length,
        client: 'gws-wiz-serp',
        hl: 'en-US',
        authuser: 0,
        pq: basic_kw,
        dpr: 1,
        ofp: ''
    }
    params["cp"] = 0; //可以得到非kw开头的其他词
    //以下是默认提示词(含问题), todo: 插件无权共享宿主页面的window数据
    var ofp = window.google && window.google.pmc && window.google.pmc.sb_wiz ? window.google.pmc.sb_wiz.onf : "";
    if(ofp) {
        params["ofp"] = ofp;
        params["cp"] = 0;
        params["client"] = "desktop-gws-wiz-on-focus-serp";
    }

    $.ajax({
        url: url,
        type: "GET",
        data: params,
        success:function (msg) {
            if(msg) {
                callback && callback(true, msg);
            } else {
                callback && callback(false, "网络错误或远程连接失败");
            }
        },
        fail: function(msg) {
            callback && callback(false);
        },
        error: function (msg) {
            callback && callback(false);
        },
    });
}

//解析搜索词结果
Google.prototype.formated = function(keyword, res) {
    /**
     * )
        ]
        }'
        [[["bottles",0,[512,273,650]],["bottles\u003cb\u003ehop\u003c\/b\u003e",0,[512,273,650]],["bottles\u003cb\u003e and bottles\u003c\/b\u003e",0,[512]],["\u003cb\u003ebottle shop\u003c\/b\u003e\u003cb\u003e hk\u003c\/b\u003e",0,[512,10]],["bottles\u003cb\u003e japan\u003c\/b\u003e",0,[512]],["bottles\u003cb\u003e of wine\u003c\/b\u003e",0,[512]],["bottless taipei",46,[512,199,175],{"zah":"[[null,null,null,[[\"bottless taipei\"],[\"Bottless 非瓶 · Songshan District, Taipei City, Taiwan\"],null,[[\"https://lh5.googleusercontent.com/p/AF1QipPvH48q8uSmhHCUuGOy2LG5aTZtrTMN5Nn7RVvF\\u003dw200-h200-n-k-no\",1]]]]]","zp":{"gs_ssp":"eJzj4tVP1zc0zDJMM4w3yjAxYLRSNagwNjExSkwyNk1ONDO0NEoxtDKoSDYyMzE2MTMzMbM0N7MwT_PiT8ovKclJLS5WKEnMLEjNBACZWRP3"}}],["bottles\u003cb\u003e and bites\u003c\/b\u003e",0,[512]],["bottles\u003cb\u003eumo\u003c\/b\u003e",0,[512]],["bottles\u003cb\u003e and bites hk\u003c\/b\u003e",0,[512]]],{"ag":{"a":{"40024":["","",1,20]}},"l":"1","q":"RXnWiKqbchkamV0xH9Lxoz8BqDs"}]
    */
    let str = res.substring(res.indexOf('"') - 2, res.lastIndexOf(']]],') + 2);
    if(!str) {
        return [];
    }
    let parts = str.split('],['),
        kw = "",
        words = [];
    for(let part of parts) {
        kw = part.split(',')[0];
        if(!kw || kw.indexOf(keyword) === -1) {
            continue;
        }
        if(kw.indexOf('"') > -1) {
            kw = kw.substring(kw.indexOf('"') + 1, kw.lastIndexOf('"'));
        }
        if(kw.indexOf('003') > -1) {
            kw = kw.replaceAll('\\u003cb', '')
                .replaceAll('\\u003c', '')
                .replaceAll('\\u003e', '')
                .replaceAll('\\u003', '')
                .replaceAll('/b', '')
                .replaceAll('\\', '')
                .trim();
        }
        if(kw == keyword) {
            continue;
        }
        words.push(kw);
    }
    return words;
}


//搜索趋势查看链接
Google.prototype.trends = function(kw) {
    var url = 'https://trends.google.com/trends/explore?q='+ kw +'&date=now%201-d&hl=en';
    var style = 'margin-left: 15px;';
    var a = '<a href="'+ url +'" style="'+ style +'" target="_blank" title="查看搜索趋势">\
        <img style="width: 16px; height: 16px" src="https://ssl.gstatic.com/trends_nrtr/3700_RC01/favicon.ico" />\
        </a>';
    return a;
}

//把请求结果展示
Google.prototype.append = function(words) {
    var html = "";
    for(let kw of words) {
        html += '<li class="kw-item"><span>'+ kw +'</span>'+ this.trends(kw) +'</li>';
    }
    $("#kw-extend").append(html);
}

//首页(主关键词)关联词
Google.prototype.relatedWords = function() {
    var el = $("#bres a");
    if(el.length === 0) {
        return;
    }
    var items = [];
    var a = null,
        kw = "";
    const _this = this;
    el.each(function(i, n) {
        kw = $(this).text();
        a = '<li>\
            <a href="'+ $(this).attr("href") +'" target="_blank">'+ kw +'</a>\
            '+ _this.trends(kw) +'\
            </li>';
        items.push(a);
    });
    $("#kw-related").append(items.join(''));
}

//首页(主关键词)问答
Google.prototype.relatedFaq = function() {
    var el = $("div.related-question-pair");
    if(el.length === 0) {
        return;
    }
    var items = [];
    var a = null,
        span = null;
    el.each(function(i, n) {
        span = $(this).find("span").first();
        if(span.length === 0) {
            return true;
        }
        a = '<li><span">'+ span.text() +'</span></li>';
        items.push(a);
    });
    $("#kw-related").append(items.join(''));
}

Google.prototype.copy = function() {
    let items = [];
    $("#kw-drawer li").each(function(i, n) {
        items.push($(this).text().trim());
    });
    let textValue = items.join('\n');
    navigator.clipboard.writeText(textValue).then(() => {
        console.log('复制成功');
    });
}

Google.prototype.toggleExtend = function() {
    if($("#kw-drawer").is(":hidden")) {
        $("#kw-drawer").show();
        $("#kw-show").text("隐藏");
    } else {
        $("#kw-drawer").hide();
        $("#kw-show").text("展示");
    }
}

Google.prototype.show = function() {
    if($("#kw-drawer").is(":hidden")) {
        $("#kw-drawer").show();
        $("#kw-show").text("隐藏");
    }
}


module.exports = Google;