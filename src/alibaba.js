const {getUrlParams, formatImageUrl} = require("./helper");

var Alibaba = function() {
};

//消息提示
Alibaba.$msgbox = null;

//监听事件
Alibaba.prototype.onload = function($msgbox) {
    var moreBtn = $("span.sku-wrapper-expend-button").first();
    if(moreBtn && moreBtn.length > 0) {
        moreBtn.click();
    }

    Alibaba.$msgbox = $msgbox;
}

//获取产品信息
Alibaba.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.title = this.getTitle();
    product.price = this.getPrice();
    product.regular_price = product.price;
    product.currency = "CNY";
    product.gallary = this.getGallary();
    product.image = product.gallary.length > 0 ? product.gallary[0] : "";
    product.video = this.getVideo();
    product.attributes = this.getAttributes();
    product.brand = this.filterBrand(product.attributes);
    product.category = this.getCategory();
    product.desc_images = this.getDescImages();
    product.description = this.getDescription();
    product.options = this.getSkuOptions();
    product.sku_list = this.getSkuList();
    product.referer = this.getReferer();
    product.source = this.getDomain();
    product.language = this.getLanguage();
    product.store_location = this.getLocation();
    product.faq_list = [];
    product.sales = 0;
    return product;
}

Alibaba.prototype.getReferer = function() {
    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
    return url;
}

Alibaba.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

Alibaba.prototype.getLanguage = function() {
    return 'zh';
}

Alibaba.prototype.getProductId = function() {
    var pathname = document.location.pathname;
    if(pathname.length < 5 ) {
        return "";
    }
    let p = pathname.match(/\d+/g);
    if(p.length === 0) {
        return "";
    }
    return p[0];
}

Alibaba.prototype.getTitle = function() {
    var el = $("#root-container").find("h1");
    if(el.length === 0 ) {
        return document.title;
    }
    return el.first().text().trim();
}

Alibaba.prototype.getPrice = function() {
    var el = $("#root-container").find("span[class='price-text']");
    if(el.length === 0 ) {
        return ""
    }
    return el.first().text();
}

Alibaba.prototype.getSku = function() {
    return this.getProductId();
}

//多个sku
Alibaba.prototype.getSkuOptions = function() {
    var results = [];
    //属性选项
    var prop_name = $("#root-container .sku-prop-module").find("div.sku-prop-module-name").text().replace("：", "").trimEnd(":", "");
    if(prop_name) {
        let items = $("#root-container .sku-prop-module").find("div.prop-item");
        if(items.length > 0 ) {
            let options = [];
            let option = "";
            items.each(function(i, n) {
                option = $(this).find(".prop-name").text().trim();
                options.push(option);
            })
            results.push({name: prop_name, options: options});
        }
    }

    //sku
    var name = $("#root-container .sku-scene-wrapper").find("div.sku-prop-module-name").text().replace("：", "").trimEnd(":", "");
    if(name) {
        let items = $("#sku-count-widget-wrapper").find("div.sku-item-wrapper");
        if(items.length > 0 ) {
            let options = [];
            let option = "";
            items.each(function(i, n) {
                option = $(this).find(".sku-item-name").text().trim();
                options.push(option);
            })
            results.push({name: name, options: options});
        }
    }

    return results;
}

Alibaba.prototype.getSkuList = function() {
    var name = $("#root-container .sku-scene-wrapper").find("div.sku-prop-module-name").text().replace("：", "").trimEnd(":", "");
    if(!name) {
        return [];
    }
    var items = $("#sku-count-widget-wrapper").find("div.sku-item-wrapper");
    if(items.length === 0 ) {
        return [];
    }
    var results = [];
    var option = "",
        image = "",
        sku = "",
        price = "";
    items.each(function(i, n) {
        option = $(this).find(".sku-item-name").text().trim();
        if(!option) {
            return true;
        }

        if($(this).find(".sku-item-image").length > 0) {
            image = $(this).find(".sku-item-image").css('background-image');
            if(image.indexOf("http") > -1) {
                image = image.substring(image.indexOf("http"), image.lastIndexOf(".jpg") + 4);
            }
        }

        price = $(this).find(".discountPrice-price").text().trim();
        if(price) {
            var p = price.match(/\d+\.\d+/g);
            price = p.length > 0 ? p[0] : "";
        }
        results.push({options: [{name: name, option: option}], image: image, price: price});
    });
    return results;
}

Alibaba.prototype.getGallary = function() {
    var imgs = $("#root-container .detail-gallery-turn").find("img.detail-gallery-img");
    if(imgs.length === 0 ) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        if(i === 0) {
            //排除视频封面
            if($(item).parent().find("img.video-icon").length === 0) {
                return true;
            }
        }
        link = _this.formatImage(item.src);
        if(urls.indexOf(link) === -1) {
            urls.push(link);
        }
    })
    return urls;
}

Alibaba.prototype.getCurrentImage = function() {
    var el = $("#root-container").find("img.preview-img");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Alibaba.prototype.getVideo = function() {
    var el = $("#root-container").find("video.lib-video");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Alibaba.prototype.getAttributes = function() {
    var el = $("#root-container").find("div.offer-attr");
    if(el.length === 0 ) {
        return [];
    }
    var attrs = el.find("div.offer-attr-item");
    if(attrs.length === 0) {
        return [];
    }
    var data = [],
        k = "",
        v = "";
    attrs.each(function(i, item) {
        k = $(this).find("span.offer-attr-item-name").text();
        v = $(this).find("span.offer-attr-item-value").text();
        data.push({key: k, value: v});
    })
    return data;
}

Alibaba.prototype.filterBrand = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "品牌") {
            return attributes[i].value;
        }
    }
    return "";
}

Alibaba.prototype.getCategory = function() {
    var el = $("ul.category-tree");
    if(el.length === 0) {
        return "";
    }
    var first = el.find("li").first();
    if(first.length === 0) {
        return "";
    }
    return first.find("a.primary-row-link").text().trim();
}


Alibaba.prototype.getDescImages = function() {
    var el = $("#root-container").find("div.detail-desc-module");
    if(el.length === 0 ) {
        return [];
    }
    var content = el.find("div.content-detail");
    if(content.length === 0 ) {
        return [];
    }
    var imgs = content.find("img");
    if(imgs.length === 0) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = $(item).attr("data-lazyload-src");
        if(!link) {
            return true;
        }
        link = _this.formatImage(link);
        urls.push(link)
    })
    return urls;
}

Alibaba.prototype.getDescription = function() {
    var el = $("#root-container").find("div.detail-desc-module");
    if(el.length === 0 ) {
        return "";
    }
    var text = el.find(".content-detail").text();
    if(!text) {
        return "";
    }
    var results = [];
    var blocks = text.split("\n");
    for(var i=0; i<blocks.length; i++) {
        blocks[i] = blocks[i].trim();
        if(!blocks[i]) {
            continue;
        }
        results.push(blocks[i]);
    }
    if(results.length === 0) {
        return "";
    }
    text = results.join("\n");
    return text.length > 20 ? text : "";
}

//发货地
Alibaba.prototype.getLocation = function() {
    var el = $("#root-container").find("div.logistics-content");
    if(el.length === 0 ) {
        return "";
    }
    var loc = el.find("span.logistics-city");
    return loc.text();
}

//格式化图片地址
Alibaba.prototype.formatImage = function(link) {
    if(!link) {
        return "";
    }
    if(link.indexOf(".jpg") > -1 && link.lastIndexOf(".webp") > -1) {
        link = link.substring(0, link.indexOf(".jpg") + 4);
    }
    return formatImageUrl(link);
}

module.exports = Alibaba;