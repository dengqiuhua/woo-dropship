const {sendFaq, formatImageUrl, extractMoney, isContainChinese, getCookie} = require("./helper");

var Aliexpress = function() {
};

//消息提示
Aliexpress.$msgbox = null;

//监听事件
Aliexpress.prototype.onload = function($msgbox) {
    Aliexpress.$msgbox = $msgbox;
    //加载描述
    window.location.hash = "#nav-description";
}

//获取产品信息
Aliexpress.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.title = this.getTitle();
    product.price = this.getPrice();
    product.regular_price = product.price;
    product.currency = this.getCurrency();
    product.gallary = this.getGallary();
    product.image = product.gallary.length > 0 ? product.gallary[0] : "";
    product.video = this.getVideo();
    product.attributes = this.getAttributes();
    product.brand = this.filterBrand(product.attributes);
    product.category = "";
    product.desc_images = this.getDescImages();
    product.description = this.getDescription();
    product.options = this.getSkuOptions();
    product.sku_list = this.getSkuList();
    product.referer = this.getReferer();
    product.source = this.getDomain();
    product.language = this.getLanguage();
    product.store_location = this.getLocation(product.attributes);
    product.faq_list = [];
    product.sales = 0;

    //主动获取faq
    this.sendFaq();
    return product;
}

Aliexpress.prototype.getReferer = function() {
    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
    return url;
}

Aliexpress.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

Aliexpress.prototype.getLanguage = function() {
    var lang = $("html").attr("lang");
    if(lang) {
        return lang.toLowerCase();
    }
    var host = location.hostname;
    var parts = host.split('.');
    if(parts.length !== 3 || parts[0] === "www") {
        let title = this.getTitle();
        if(isContainChinese(title)) {
            return "zh";
        }
        return "en";
    }
    return parts[0];
}

Aliexpress.prototype.getCurrency = function() {
    return getCookie('aep_usuc_f', 'c_tp') || 'USD';
}

Aliexpress.prototype.getProductId = function() {
    var pathname = document.location.pathname;
    if(pathname.length < 5 ) {
        return "";
    }
    let p = pathname.match(/\d+/g);
    if(p.length === 0) {
        return "";
    }
    return p[0];
}

Aliexpress.prototype.getTitle = function() {
    var el = $("#root").find("h1");
    if(el.length === 0 ) {
        return document.title;
    }
    return el.first().text().trim();
}

Aliexpress.prototype.getPrice = function() {
    var el = $("#root").find("span[class^='price--originalText']");
    if(el.length === 0 ) {
        return ""
    }
    var val = el.first().text();
    return extractMoney(val);
}

Aliexpress.prototype.getSku = function() {
    return this.getProductId();
}

//多个sku
Aliexpress.prototype.getSkuOptions = function() {
    var el = $("#root").find("div[class^='sku-item--skus']");
    if(el.length === 0 ) {
        return [];
    }
    var items = el.find('img')
    var name = $("#root").find("div[class^='sku-item--title']").text();
    if(name.indexOf(':') > -1) {
        name = name.substring(0, name.indexOf(':'));
    }
    var results = [];
    var options = [];
    var option = "";
    items.each(function(i, n) {
        option = $(this).attr("alt").trim();
        options.push(option);
    })
    results.push({name: name, options: options});
    return results;
}

Aliexpress.prototype.getSkuList = function() {
    var el = $("#root").find("div[class^='sku-item--skus']");
    if(el.length === 0 ) {
        return [];
    }
    var items = el.find('img')
    var name = $("#root").find("div[class^='sku-item--title']").text();
    if(name.indexOf(':') > -1) {
        name = name.substring(0, name.indexOf(':'));
    }
    var results = [];
    var options = [];
    var option = "",
        image = "",
        sku = "",
        price = "",
        _this = this;
    items.each(function(i, n) {
        option = $(this).attr("alt").trim();
        image = _this.formatImage($(this).attr("src"));
        price = "";
        if(price) {
            price = extractMoney(price);
        }
        options.push({name: name, option: option});
        results.push({options: options, image: image, price: price});
    });
    return results;
}

Aliexpress.prototype.getGallary = function() {
    var el = $("#root").find("div[class^='slider--slider']").first();
    if(!el || el.length === 0) {
        return [];
    }
    var imgs = el.find("img");
    if(imgs.length === 0 ) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = _this.formatImage(item.src);
        if(urls.indexOf(link) === -1) {
            urls.push(link);
        }
    })
    return urls;
}

Aliexpress.prototype.getCurrentImage = function() {
    var el = $("#root").find("img[class^='magnifier--image']");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Aliexpress.prototype.getVideo = function() {
    var el = $("#root").find("video.lib-video");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Aliexpress.prototype.getAttributes = function() {
    var el = $("#root").find("ul[class^='specification--list']");
    if(el.length === 0 ) {
        return [];
    }
    var attrs = el.find("div[class^='specification--prop']");
    if(attrs.length === 0) {
        return [];
    }
    var data = [],
        k = "",
        v = "";
    attrs.each(function(i, item) {
        k = $(this).find("div[class^='specification--title']").text();
        v = $(this).find("div[class^='specification--desc']").text();
        data.push({key: k, value: v});
    })
    return data;
}

Aliexpress.prototype.filterBrand = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "Brand Name") {
            return attributes[i].value;
        }
    }
    return "";
}

Aliexpress.prototype.getDescImages = function() {
    var el = $("#product-description").find("p");
    if(el.length === 0 ) {
        return [];
    }
    var imgs = el.find("img");
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        if(!$(item).attr("class") === "desc-img-loaded") {
            return true;
        }
        link = $(item).attr("src");
        if(!link) {
            return true;
        }
        link = _this.formatImage(link);
        urls.push(link)
    })
    return urls;
}

Aliexpress.prototype.getDescription = function() {
    var el = $("#product-description").find("p");
    if(el.length === 0 ) {
        return "";
    }

    var results = [];
    for(var i=0; i<el.length; i++) {
        results.push($(el[i]).text().trim());
    }
    if(results.length === 0) {
        return "";
    }
    text = results.join("\n");
    return text.length > 20 ? text : "";
}

//发货地
Aliexpress.prototype.getLocation = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    var loc = [];
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "Origin") {
            loc.push(attributes[i].value);
        }
        if(attributes[i].key === "CN") {
            loc.push(attributes[i].value);
        }
    }
    if(loc.length === 0) {
        return "";
    }
    return loc.join(",");
}


//获取faq并发送
Aliexpress.prototype.sendFaq = function() {
    try {
        var source = this.getDomain();
        var product_id = this.getProductId();
        var url = "https://www.aliexpress.com/aeglodetailweb/api/questions?productId="+ product_id +"&currentPage=1&pageSize=10";
        $.get(url, function (msg) {
            if(!msg || msg.code != 200 || !msg.body || !msg.body.questionList) {
                return
            }
            var results = [];
            for(let item of msg.body.questionList) {
                if(!item.content || !item.answers || item.answers.length === 0) {
                    continue;
                }
                results.push({
                    question: item.content,
                    answer: item.answers[0].content
                });
            };
            if(results.length === 0) {
                return
            }
            sendFaq({product_id: product_id, source: source, faq_list: results});
        });
    } catch(e) {
        console.log("[faq] fail, err:"+ e);
    }
}

//格式化图片地址
Aliexpress.prototype.formatImage = function(link) {
    if(!link) {
        return "";
    }
    if(link.indexOf(".jpg") > -1 && link.lastIndexOf(".webp") > -1) {
        link = link.substring(0, link.indexOf(".jpg") + 4);
    }
    return formatImageUrl(link);
}

module.exports = Aliexpress;