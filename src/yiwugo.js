const {sendFaq, formatImageUrl, extractMoney} = require("./helper");

var Yiwugo = function() {
};

//消息提示
Yiwugo.$msgbox = null;

//监听事件
Yiwugo.prototype.onload = function($msgbox) {
    Yiwugo.$msgbox = $msgbox;
}

//获取产品信息
Yiwugo.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.title = this.getTitle();
    product.price = this.getPrice();
    product.regular_price = product.price;
    product.currency = "CNY";
    product.gallary = this.getGallary();
    product.image = product.gallary.length > 0 ? product.gallary[0] : "";
    product.video = this.getVideo();
    product.attributes = this.getAttributes();
    product.brand = this.filterBrand();
    product.category = this.getCategory();
    product.desc_images = this.getDescImages();
    product.description = this.getDescription();
    product.options = this.getSkuOptions();
    product.sku_list = this.getSkuList();
    product.referer = this.getReferer();
    product.source = this.getDomain();
    product.language = this.getLanguage();
    product.store_location = this.getLocation(product.attributes);
    product.faq_list = [];
    product.sales = 0;

    //主动获取faq
    this.sendFaq();
    return product;
}

Yiwugo.prototype.getReferer = function() {
    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
    return url;
}

Yiwugo.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

Yiwugo.prototype.getLanguage = function() {
    return 'zh';
}

Yiwugo.prototype.getProductId = function() {
    var pathname = document.location.pathname;
    if(pathname.length < 5 ) {
        return "";
    }
    let p = pathname.match(/\d+/g);
    if(p.length === 0) {
        return "";
    }
    return p[0];
}

Yiwugo.prototype.getTitle = function() {
    var el = $(".title-box").find(".title");
    if(el.length === 0 || !el.text()) {
        return document.title;
    }
    var text = el.text();
    var trim = el.children().text();
    return text.replace(trim, '').replaceAll('\n', '').trim();
}

Yiwugo.prototype.getPrice = function() {
    var el = $(".picture-and-price").find(".origin-price");
    if(el.length === 0) {
        el = $(".picture-and-price").find(".price");
        if(el.length === 0) {
            return ""
        }
    }
    var val = extractMoney(el.text());
    return val;
}

Yiwugo.prototype.getSku = function() {
    return this.getProductId();
}

//多个sku
Yiwugo.prototype.getSkuOptions = function() {
    var items = $(".calculator").find("div.product-item");
    if(items.length === 0 ) {
        items = $(".calculator").find("div.item");
        if(items.length === 0) {
            return []
        }
    }
    var name = $(".calculator").find('.label').text().trim();
    if(!name) {
        return [];
    }
    var results = [];
    var options = [];
    var option = "";
    items.each(function(i, n) {
        option = $(this).find('.props').text().trim();
        if(!option) {
            option = $(this).find('.name').text().trim();
        }
        if(!option) {
            return true;
        }
        options.push(option);
    })
    results.push({name: name, options: options});
    return results;
}

Yiwugo.prototype.getSkuList = function() {
    var items = $(".calculator").find("div.product-item");
    if(items.length === 0 ) {
        items = $(".calculator").find("div.item");
        if(items.length === 0) {
            return []
        }
        return [];
    }
    var name = $(".calculator").find('.label').text().trim();
    if(!name) {
        return [];
    }
    var results = [];
    var options = [];
    var option = "",
        image = "",
        price = "",
        _this = this;
    items.each(function(i, n) {
        option = $(this).find('.props').text().trim();
        if(!option) {
            option = $(this).find('.name').text().trim();
        }
        if(!option) {
            return true;
        }
        price = $(this).find('.fb').text().trim();
        if(price) {
            price = extractMoney(price);
        }
        options.push({name: name, option: option});
        results.push({options: options, image: image, price: price});
    });
    return results;
}

Yiwugo.prototype.getGallary = function() {
    var el = $(".preview-row").find("div.swiper-wrapper").first();
    if(!el || el.length === 0) {
        return [];
    }
    var imgs = el.find("img");
    if(imgs.length === 0 ) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = _this.formatImage($(item).attr('data-src'));
        if(urls.indexOf(link) === -1) {
            urls.push(link);
        }
    })
    return urls;
}

Yiwugo.prototype.getCurrentImage = function() {
    var el = $("#picZoom").find("img.img");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Yiwugo.prototype.getVideo = function() {
    var el = $("#root").find("video.lib-video");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Yiwugo.prototype.getAttributes = function() {
    return [];
}

Yiwugo.prototype.filterBrand = function() {

    return "";
}

Yiwugo.prototype.getCategory = function() {
    var el = $(".classify").find(".classify-link");
    if(el.length === 0 ) {
        return "";
    }
    return el.last().find('a').text().trim();
}

Yiwugo.prototype.getDescImages = function() {
    var el = $(".tab-detail").find(".cpxq");
    if(el.length === 0 ) {
        return [];
    }
    var imgs = el.find("img");
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = $(item).attr("src");
        if(!link) {
            return true;
        }
        link = _this.formatImage(link);
        urls.push(link)
    })
    return urls;
}

Yiwugo.prototype.getDescription = function() {
    return ""
}

//发货地
Yiwugo.prototype.getLocation = function() {
    var el = $(".store-info").find('.location-icon');
    if(el.length === 0 ) {
        return "";
    }
    return el.next().text().trim();
}


//获取faq并发送
Yiwugo.prototype.sendFaq = function() {
    try {
        //无
    } catch(e) {
        console.log("[faq] fail, err:"+ e);
    }
}

//格式化图片地址
Yiwugo.prototype.formatImage = function(link) {
    if(!link) {
        return "";
    }
    var p = link.indexOf('?');
    if(p > -1) {
        link = link.substring(0, p);
    }
    return formatImageUrl(link);
}

module.exports = Yiwugo;