const {getUrlParams} = require("./helper");

var TaobaoFaq = function() {
};

TaobaoFaq.$msgbox = null;

//监听事件
TaobaoFaq.prototype.onload = function($msgbox) {
    if($msgbox) {
        var count = this.getFaqCount();
        var html = "";
        if(count) {
            html = "发现"+ count + "个问题, ";
        }
        html += "提交faq";
        $msgbox.html(html);
    }
    TaobaoFaq.$msgbox = $msgbox;
}

//获取产品信息
TaobaoFaq.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.source = this.getDomain();
    product.faq_list = this.getFaqList();
    return product;
}

TaobaoFaq.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

TaobaoFaq.prototype.getProductId = function() {
    return getUrlParams("refId");
}

TaobaoFaq.prototype.getFaqList = function() {
    var el = $("div.rax-scrollview-webcontainer").children(".rax-view-v2").last();
    if(el.length === 0) {
        return [];
    }
    var qa = el.children();
    var results = [];
    qa.each(function(i) {
        var q = $(this).find("div").first().text().trim();
        var a = $(this).children().eq(1).find("span").last().text().trim();
        if(!q || !a) {
            return true;
        }
        results.push({
            question: q,
            answer: a
        })
    })
    return results;
}

TaobaoFaq.prototype.getFaqCount = function() {
    var text = $("div[data-spm=questionlist]").find("span.rax-text-v2 ").text();
    if(!text) {
        return "";
    }
    return text.match(/\d+/g);
}


module.exports = TaobaoFaq;