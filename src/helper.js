//获取URL参数
function getUrlParams(key) {
    let url = window.location.href;
    if(url.indexOf('?') === -1) {
        return key ? '' : {};
    }
    let urlStr = url.split('?')[1];
    const urlSearchParams = new URLSearchParams(urlStr);
    const result = Object.fromEntries(urlSearchParams.entries());
    if(key) {
        return result[key] || "";
    }
    return result;
}

function getCookie(name, key) {
    name = name + '=';
    var cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
    var cookie = cookies[i].trim();
    if (cookie.indexOf(name) == 0) {
        var value = cookie.substring(name.length, cookie.length);
        if (key) {
        // 获取cookie中的key
        var cookieRegExp = new RegExp(
            '(.*&?' + key + '=)(.*?)(&.*|$)',
        );
        var matchArr = value.match(cookieRegExp)
        return matchArr && matchArr[2] || '';
        } else {
        // 直接获取cookie
        return value;
        }
    }
    }
    return '';
}

//上报远程地址
function getApiUrl() {
    //获取环境env
    let host = "http://127.0.0.1:3000";
    host = window.__WOO_HOST || "https://dp.whatsmail.cn";
    return host + "/api/collection/append";
}

function getFaqUrl() {
    //获取环境env
    let host = "http://127.0.0.1:3000";
    host = window.__WOO_HOST || "https://dp.whatsmail.cn";
    return host + "/api/faq/push";
}

function getAppKey() {
    // return "wo366dd64fc6645c7cbb1978a859ac56";
    //pupup页配置
    let key = window.__WOO_APPKEY;
    return key;
}

//上报采集数据
function sendProduct(data, callback) {
    let url = getApiUrl();
    if(!url) {
        return;
    }
    let key = getAppKey();
    if(!key) {
        callback(false, "AppKey未配置, 请点击右上角插件配置!");
        return;
    }
    //TODO, 数据加密
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data),
        headers: {
            appkey: key
        },
        contentType: "application/json;charset=utf-8",
        dataType:"json",
        success:function (msg) {
            if(msg && msg.code === 0) {
                callback && callback(true);
            } else {
                callback && callback(false, "网络错误或远程连接失败");
            }
        },
        fail: function() {
            callback && callback(false);
        },
        error: function () {
            callback && callback(false);
        },
    });
}

//上报采集FAQ数据
function sendFaq(data, callback) {
    let url = getFaqUrl();
    if(!url) {
        return;
    }
    let key = getAppKey();
    if(!key) {
        callback(false, "AppKey未配置, 请点击右上角插件配置!");
        return;
    }
    //TODO, 数据加密
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data),
        headers: {
            appkey: key
        },
        contentType: "application/json;charset=utf-8",
        dataType:"json",
        success:function (msg) {
            if(msg && msg.code === 0) {
                callback && callback(true);
            } else {
                callback && callback(false, "网络错误或远程连接失败");
            }
        },
        fail: function() {
            callback && callback(false);
        },
        error: function () {
            callback && callback(false);
        },
    });
}

//格式化图片地址
function formatImageUrl(link) {
    if(link.startsWith("//")) {
        link = window.location.protocol + link;
    }
    return link;
}

//随机生成字符串
function randomString(len) {
    len = len || 32;
    var t = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678",
        a = t.length,
        n = "";
    for (let i = 0; i < len; i++) n += t.charAt(Math.floor(Math.random() * a));
    return n;
}

/**
 * 是否是中文
 * @param {*} str
 * @returns
 */
function isContainChinese(str) {
    return /[\u4e00-\u9fa5]/.test(str);
}

function extractMoney(value) {
    if(!value) {
        return 0;
    }
    if(!isNaN(value)) {
        return value;
    }
    if(value.indexOf(',') > -1) {
        value = value.replaceAll(',', '');
    }
    if(!isNaN(value)) {
        return value;
    }
    var p = value.match(/\d+\.\d+/g);
    return p && p.length > 0 ? p[0] : 0;
}

module.exports = {
    sendProduct,
    sendFaq,
    getUrlParams,
    getCookie,
    formatImageUrl,
    randomString,
    isContainChinese,
    extractMoney
};
