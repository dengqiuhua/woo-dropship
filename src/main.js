const Site = require("./site");

//注入js
function addScript(src) {
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.src = chrome.runtime.getURL(src);
    (document.body || document.head || document.documentElement).appendChild(script);
}

addScript("./js/jquery.min.js");


(function() {

    window.__WOO_APPKEY = "";
    window.__WOO_HOST = "";

    //发送消息
    var sendBackground = function(message) {
        chrome.runtime && chrome.runtime.sendMessage(message, function(response) {
            // console.log('[woo]收到来自background的回复：', response);
            if(message.event === 'getConfig') {
                if(response && typeof response === 'object') {
                    window.__WOO_APPKEY = response.key;
                    window.__WOO_HOST = response.host;
                }

            }
        });
    }

    window.onload = function() {
        setTimeout(() => {
            var site = new Site();
            site.onLoad();
        }, 1200);
    }


    //获取自定义key配置
    sendBackground({event: 'getConfig'});

    //加载时更新插件
    //sendBackground({event: 'reload'});

})();
