const {formatImageUrl, sendFaq, extractMoney} = require("./helper");

var Jd = function() {
};

//消息提示
Jd.$msgbox = null;

//监听事件
Jd.prototype.onload = function($msgbox) {
    var moreBtn = $("span.sku-wrapper-expend-button").first();
    if(moreBtn && moreBtn.length > 0) {
        moreBtn.click();
    }
    //监听tab切换
    //$("#detail .tab-main li").click(this.onTab);

    Jd.$msgbox = $msgbox;
}

//获取产品信息
Jd.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.title = this.getTitle();
    product.price = this.getPrice();
    product.regular_price = product.price;
    product.currency = "CNY";
    product.gallary = this.getGallary();
    product.image = product.gallary.length > 0 ? product.gallary[0] : "";
    product.video = this.getVideo();
    product.attributes = this.getAttributes();
    product.brand = this.filterBrand(product.attributes);
    product.category = this.getCategory();
    product.desc_images = this.getDescImages();
    product.description = this.getDescription();
    product.options = this.getSkuOptions();
    product.sku_list = this.getSkuList();
    product.referer = this.getReferer();
    product.source = this.getDomain();
    product.language = this.getLanguage();
    product.store_location = this.getLocation();
    // product.faq_list = this.getFaqList();
    product.sales = 0;

    //主动获取faq
    this.sendFaq();
    return product;
}

Jd.prototype.getReferer = function() {
    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
    return url;
}

Jd.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

Jd.prototype.getLanguage = function() {
    return 'zh';
}

Jd.prototype.getProductId = function() {
    var pathname = document.location.pathname;
    if(pathname.length < 5 ) {
        return "";
    }
    let p = pathname.match(/\d+/g);
    if(p.length === 0) {
        return "";
    }
    return p[0];
}

Jd.prototype.getTitle = function() {
    var el = $("div.product-intro").find(".sku-name");
    if(el.length === 0 ) {
        return document.title;
    }
    return el.first().text().trim();
}

Jd.prototype.getPrice = function() {
    var el = $("div.product-intro .p-price").find("span.price");
    if(el.length === 0 ) {
        return ""
    }
    return extractMoney(el.first().text());
}

Jd.prototype.getSku = function() {
    return this.getProductId();
}

//多个sku
Jd.prototype.getSkuOptions = function() {
    var el = $("#choose-attrs").find(".p-choose");
    if(el.length === 0 ) {
        return [];
    }
    var results = [];
    for(var i=0; i < el.length; i++) {
        var items = $(el[i]).find("div.item");
        if(items.length === 0) {
            continue;
        }
        var options = [];
        items.each(function(i, n) {
            options.push($(this).attr("data-value"));
        })
        var name = $(el[i]).attr("data-type");
        results.push({name: name, options: options});
    }
    return results;
}

Jd.prototype.getSkuList = function() {
    var el = $("#choose-attrs").find(".p-choose");
    if(el.length === 0 ) {
        return [];
    }
    var results = [];
    for(var i=0; i < el.length; i++) {
        var items = $(el[i]).find("div.item");
        if(items.length === 0) {
            continue;
        }
        var options = [];
        var option = "",
            image = "",
            sku = "",
            price = "";
        var name = $(el[i]).attr("data-type");

        items.each(function(i, n) {
            sku = $(this).attr("data-sku");
            image = $(this).find("img").attr("src");
            if(image && image.indexOf("n9/") > -1) {
                var host = image.substring(0, image.indexOf("n9/")) + "n1/";
                image = host + image.substring(image.indexOf("jfs/"), image.length);
            }
            if(!sku && !image) {
                return true
            }
            option = $(this).attr("data-value");
            price = "";
            options.push({name: name, option: option});
            results.push({ options: options, image: image, price: price, sku: sku});
        });
    }
    return results;
}

Jd.prototype.getGallary = function() {
    var imgs = $("#spec-list").find("img");
    if(imgs.length === 0 ) {
        return [];
    }
    var urls = [],
        link = "",
        src = "",
        _this = this;
    imgs.each(function(i, item) {
        src = $(this).attr("src");
        link = src.substring(0, src.indexOf("n5")) + "n1/" + $(this).attr("data-url");
        link = _this.formatImage(link);
        if(urls.indexOf(link) === -1) {
            urls.push(link);
        }
    })
    return urls;
}

Jd.prototype.getCurrentImage = function() {
    var el = $("div.product-intro").find("img#spec-img");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Jd.prototype.getVideo = function() {
    var el = $("div.product-intro").find("video");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Jd.prototype.getAttributes = function() {
    var el = $("#detail").find("ul.p-parameter-list");
    if(el.length === 0 ) {
        return [];
    }
    var attrs = el.find("li");
    if(attrs.length === 0) {
        return [];
    }
    var data = [],
        kv = null;
    attrs.each(function(i, item) {
        kv = item.innerText.replace("：", ":").split(":");
        data.push({key: kv[0], value: kv[1]});
    })
    return data;
}

Jd.prototype.filterBrand = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "品牌") {
            return attributes[i].value;
        }
    }
    return "";
}

Jd.prototype.getCategory = function() {
    var first = $("#crumb-wrap").find("div.first").text().trim();
    return first;
}

Jd.prototype.getDescImages = function() {
    var imgs = $("#J-detail-content").find("img");
    if(imgs.length === 0 ) {
        return [];
    }
    if(imgs.length === 0) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = $(this).attr("data-lazyload");
        if(!link) {
            link = $(this).attr("src");
            if(!link || link.indexOf(".gif") > -1) {
                return true;
            }
        }
        link = _this.formatImage(link);
        urls.push(link)
    })
    return urls;
}

Jd.prototype.getDescription = function() {
    var el = $("#J-detail-content");
    if(el.length === 0 ) {
        return "";
    }
    var text = el.text();
    if(!text) {
        return "";
    }
    var results = [];
    var blocks = text.split("\n");
    for(var i=0; i<blocks.length; i++) {
        blocks[i] = blocks[i].trim();
        if(!blocks[i]) {
            continue;
        }
        results.push(blocks[i]);
    }
    if(results.length === 0) {
        return "";
    }
    text = results.join("\n");
    return text.length > 20 ? text : "";
}

//发货地
Jd.prototype.getLocation = function() {
    return ""
}

Jd.prototype.onTab = function() {
    if($(this).attr("data-anchor") !== "#comment") {
        return
    }
    var faq = new Jd().getFaqList();
    if(faq && faq.length > 0) {
        Jd.$msgbox && Jd.$msgbox.host("发现"+ results.length +"个问题");
    }
}

Jd.prototype.getFaqList = function() {
    var items = $("#askAnswer").find("div.askAnswer-item");
    if(items.length === 0) {
        return []
    }

    var results = [];
    items.each(function(i) {
        var q = $(this).find('div.ask').find("p").text().trim();
        var a = $(this).find('div.answer').find("p").text().trim();
        results.push({
            question: q,
            answers: a
        })
    })
    return results;
}

//获取faq并发送
Jd.prototype.sendFaq = function() {
    try {
        var source = this.getDomain();
        var product_id = this.getProductId();
        var url = "https://api.m.jd.com/?appid=item-v3&functionId=getQuestionAnswerList&client=pc&clientVersion=1.0.0&t="+ Date.now() +"&loginType=3&page=1&productId="+ product_id;
        $.get(url, function (msg) {
            if(!msg || msg.resultCode != 0 || !msg.questionList) {
                return
            }
            var results = [];
            for(let item of msg.questionList) {
                if(!item.content || !item.answerList || item.answerList.length === 0) {
                    continue;
                }
                results.push({
                    question: item.content,
                    answer: item.answerList[0].content
                });
            };
            if(results.length === 0) {
                return
            }
            sendFaq({product_id: product_id, source: source, faq_list: results});
        });
    } catch(e) {
        console.log("[faq] fail, err:"+ e);
    }
}

//格式化图片地址
Jd.prototype.formatImage = function(link) {
    if(!link) {
        return "";
    }
    if(link.indexOf(".jpg") > -1 && link.lastIndexOf(".avif") > -1) {
        link = link.substring(0, link.indexOf(".jpg") + 4);
    }
    return formatImageUrl(link);
}

module.exports = Jd;