const {sendFaq, formatImageUrl, getUrlParams, isContainChinese, extractMoney} = require("./helper");

var Amazon = function() {
};

//消息提示
Amazon.$msgbox = null;

//监听事件
Amazon.prototype.onload = function($msgbox) {
    Amazon.$msgbox = $msgbox;
}

//获取产品信息
Amazon.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.title = this.getTitle();
    product.price = this.getPrice();
    product.regular_price = product.price;
    product.currency = this.getCurrency();
    product.gallary = this.getGallary();
    product.image = product.gallary.length > 0 ? product.gallary[0] : "";
    product.video = this.getVideo();
    product.attributes = this.getAttributes();
    product.brand = this.filterBrand(product.attributes);
    product.category = "";
    product.desc_images = this.getDescImages();
    product.description = this.getDescription();
    product.options = this.getSkuOptions();
    product.sku_list = this.getSkuList();
    product.referer = this.getReferer();
    product.source = this.getDomain();
    product.language = this.getLanguage();
    product.store_location = this.getLocation(product.attributes);
    product.faq_list = [];
    product.sales = 0;

    //主动获取faq
    this.sendFaq();
    return product;
}

Amazon.prototype.getReferer = function() {
    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
    return url;
}

Amazon.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

Amazon.prototype.getLanguage = function() {
    var lang = "";
    var el = $("#nav-tools .nav-line-2").find('div');
    if(el.length > 0) {
        return el.text().trim().toLowerCase();
    }
    lang = getUrlParams("language") || $("html").attr("lang");
    if(lang) {
        return lang.split('_')[0].toLowerCase();
    }
    let title = this.getTitle();
    if(isContainChinese(title)) {
        return 'zh';
    }
    return 'en';
}

Amazon.prototype.getCurrency = function() {
    var el = $("input[name=currencyOfPreference]");
    if(el.length > 0) {
        return el.val();
    }
    return 'USD';
}

Amazon.prototype.getProductId = function() {
    var el = $("#asin");
    if(el.length > 0 && el.val()) {
        return el.val();
    }
    var pathname = document.location.pathname;
    if(pathname.length < 5 ) {
        return "";
    }
    return this.getAmazonAsinFromUrl(pathname);
}

Amazon.prototype.getTitle = function() {
    var el = $("#dp").find("h1");
    if(el.length === 0 ) {
        return document.title;
    }
    return el.first().text().trim();
}

Amazon.prototype.getPrice = function() {
    var el = $("#dp").find("#corePriceDisplay_desktop_feature_div");
    if(el.length === 0 ) {
        el = $("#dp").find("#corePrice_desktop")
        if(el.length === 0 ) {
            return "";
        }
    }
    var val = el.find("span.a-price").text();
    if(val) {
        val = extractMoney(val);
    }
    return val;
}

Amazon.prototype.getSku = function() {
    return this.getProductId();
}

//多个sku
Amazon.prototype.getSkuOptions = function() {
    var el = $("#dp").find("#twister");
    if(el.length === 0 ) {
        return [];
    }
    var items = el.find("div.a-section");
    if(items.length === 0) {
        return []
    }
    var results = [];
    items.each(function(i, n) {
        var options = [];
        var option = "";
        var name = $(this).find('label.a-form-label').text().trim();
        if(!name) {
            return true;
        }
        var uls = $(this).find('li');
        if(uls.length > 0) {
            uls.each(function() {
                var li = $(this).find('img');
                if(li.length > 0) {
                    option = li.attr('alt');
                    options.push(option);
                    return true;
                }
                li = $(this).find('div.twisterTextDiv');
                if(li.length > 0) {
                    option = li.text().trim();
                    options.push(option);
                    return true;
                }
            });
        } else {
            uls = $(this).find('select');
            if(uls.length > 0) {
                uls.find('option').each(function() {
                    option = $(this).text().trim();
                    options.push(option);
                })
            }
        }

        if(options.length === 0) {
            return true;
        }

        results.push({name: name, options: options});
    })

    return results;
}

Amazon.prototype.getSkuList = function() {
    return []
}

Amazon.prototype.getGallary = function() {
    var el = $("#dp").find("#altImages");
    if(!el || el.length === 0) {
        return [];
    }
    var imgs = el.find("img");
    if(imgs.length === 0 ) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = _this.formatImage(item.src);
        if(link.indexOf('HomeCustomProduct') > -1) {
            return true;
        }
        if(link.indexOf('40_') > -1) {
            link = link.replace('40_', '575_');
        }
        if(urls.indexOf(link) === -1) {
            urls.push(link);
        }
    })
    return urls;
}

Amazon.prototype.getCurrentImage = function() {
    var el = $("#dp").find("img.imgTagWrapper");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Amazon.prototype.getVideo = function() {
    var el = $("#dp").find("video.lib-video");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Amazon.prototype.getAttributes = function() {
    var el = $("#dp").find("#productFactsDesktopExpander");
    if(el.length === 0 ) {
        el = $("#dp").find("#productOverview_feature_div");
        if(el.length === 0) {
            return [];
        }
    }
    var attrs = el.find("div.product-facts-detail");
    if(attrs.length === 0) {
        attrs = el.find(".a-spacing-small");
        if(attrs.length === 0) {
            return [];
        }
    }
    var data = [],
        k = "",
        v = "";
    attrs.each(function(i, item) {
        k = $(this).find("div.a-col-left").text().replaceAll('\n', '').trim();
        if(!k) {
            k = $(this).find("td").first().text().replaceAll('\n', '').trim()
        }
        v = $(this).find("div.a-col-right").text().replaceAll('\n', '').trim();
        if(!v) {
            v = $(this).find("td").last().text().replaceAll('\n', '').trim()
        }
        if(k && v) {
            data.push({key: k, value: v});
        }

    })
    return data;
}

Amazon.prototype.filterBrand = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "Brand Name" || attributes[i].key === "品牌") {
            return attributes[i].value;
        }
    }
    return "";
}

Amazon.prototype.getDescImages = function() {
    var imgs = $("#aplus").find("img");
    if(imgs.length === 0 ) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = $(item).attr("data-src");
        if(!link) {
            link = $(item).attr("src");
            if(!link) {
                return true;
            }
        }
        if(link.indexOf('.gif') > -1) {
            return true;
        }
        link = _this.formatImage(link);
        urls.push(link)
    })
    return urls;
}

Amazon.prototype.getDescription = function() {
    var el = $("#productFactsDesktop_feature_div").find("ul.a-unordered-list");
    if(el.length === 0 ) {
        el = $("#feature-bullets").find("li");
        if(el.length === 0 ) {
            return "";
        }
    }

    var results = [];
    for(var i=0; i<el.length; i++) {
        results.push($(el[i]).text().trim());
    }
    if(results.length === 0) {
        return "";
    }
    text = results.join("\n");
    return text.length > 20 ? text : "";
}

//发货地
Amazon.prototype.getLocation = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    var loc = [];
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "Origin") {
            loc.push(attributes[i].value);
        }
        if(attributes[i].key === "CN") {
            loc.push(attributes[i].value);
        }
    }
    if(loc.length === 0) {
        return "";
    }
    return loc.join(",");
}


//获取faq并发送
Amazon.prototype.sendFaq = function() {
    try {
        var source = this.getDomain();
        var product_id = this.getProductId();
        var url = "https://www.Amazon.com/aeglodetailweb/api/questions?productId="+ product_id +"&currentPage=1&pageSize=10";
        $.get(url, function (msg) {
            if(!msg || msg.code != 200 || !msg.body || !msg.body.questionList) {
                return
            }
            var results = [];
            for(let item of msg.body.questionList) {
                if(!item.content || !item.answers || item.answers.length === 0) {
                    continue;
                }
                results.push({
                    question: item.content,
                    answer: item.answers[0].content
                });
            };
            if(results.length === 0) {
                return
            }
            sendFaq({product_id: product_id, source: source, faq_list: results});
        });
    } catch(e) {
        console.log("[faq] fail, err:"+ e);
    }
}

Amazon.prototype.getAmazonAsinFromUrl = function(url) {
    const asinRegex = /(?:dp|product)\/([a-zA-Z0-9]{10})/;
    const match = url.match(asinRegex);
    return match ? match[1] : null;
}

//格式化图片地址
Amazon.prototype.formatImage = function(link) {
    if(!link) {
        return "";
    }
    if(link.indexOf(".jpg") > -1 && link.lastIndexOf(".webp") > -1) {
        link = link.substring(0, link.indexOf(".jpg") + 4);
    }
    return formatImageUrl(link);
}

module.exports = Amazon;