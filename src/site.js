const {sendProduct, sendFaq} = require("./helper");
const Taobao = require("./taobao");
const Alibaba = require("./alibaba");
const JD = require("./jd");
const TaobaoFaq = require("./taobao_faq");
const Aliexpress = require("./aliexpress");
const Yiwugo = require("./yiwugo");
const Amazon = require("./amazon");
const Google = require("./google");


var Site = function() {};

Site.prototype.onLoad = function() {
    var page = null;
    if(this.isTaobao()) {
        page = new Taobao();
    } else if(this.isAlibaba()) {
        page = new Alibaba();
    } else if(this.isJD()) {
        page = new JD();
    } else if(this.isTaobaoFaq()) {
        page = new TaobaoFaq();
    } else if(this.isAliexpress()) {
        page = new Aliexpress();
    } else if(this.isYiwugo()) {
        page = new Yiwugo();
    } else if(this.isAmazon()) {
        page = new Amazon();
    } else if(this.isGoogle()) {
        page = new Google();
    }

    if(page) {
        this.initUI();
        const $msg = this.getMessageBox();
        page.onload($msg);
    }
}

Site.prototype.handler = function(e) {
    var info = null,
        site = new Site();
    if(site.isTaobao()) {
        info = new Taobao().getProductInfo();
    } else if(site.isAlibaba()) {
        info = new Alibaba().getProductInfo();
    } else if(site.isJD()) {
        info = new JD().getProductInfo();
    } else if(site.isTaobaoFaq()) {
        info = new TaobaoFaq().getProductInfo();
    } else if(site.isAliexpress()) {
        info = new Aliexpress().getProductInfo();
    } else if(site.isYiwugo()) {
        info = new Yiwugo().getProductInfo();
    } else if(site.isAmazon()) {
        info = new Amazon().getProductInfo();
    } else if(site.isGoogle()) {
        new Google().suggest();
    }
    if(!info) {
        return
    }

    if(site.isTaobaoFaq()) {
        site.processing();
        sendFaq(info, function(flag, msg) {
            let site = new Site();
            if(flag) {
                site.success();
            } else {
                site.fail(msg);
            }
        });
        return
    }
    //sendBackground({event: "product", data: info});
    site.processing();
    sendProduct(info, function(flag, msg) {
        let site = new Site();
        if(flag) {
            site.success();
        }else {
            site.fail(msg);
        }
    });
}

Site.prototype.initUI = function() {
    var style = "position:fixed; bottom:2px; right: 1%; z-index: 99999; width: 360px; color: #ffffff; display:flex; justify-content:flex-start; align-items: center; background-color: #778888;"
    var html = '<div id="woo-dropbar" style="'+ style +'">\
    <div id="woo-dropbtn">'+ this.newButton() +'</div>\
    <div id="woo-dropmsg"></div>\
    '+ this.getClose() +'\
    </div>';
    $("body").append(html);
    $("#woo-dropbar").find("button").on("click", this.handler);
    $("#woo-dropbar").find("#woo-close").on("click", function() {
        $("#woo-dropbar").remove();
    })
}

Site.prototype.newButton = function() {
    var style = 'display: inline-block;\
    font-weight: 400;\
    color: #fff;\
    background-color: #007bff;\
    border-color: #007bff;\
    text-align: center;\
    vertical-align: middle;\
    -webkit-user-select: none;\
    -moz-user-select: none;\
    -ms-user-select: none;\
    user-select: none;\
    border: 1px solid transparent;\
    padding: 6px 12px;\
    margin: 0px 4.4px;\
    cursor: pointer;\
    font-size: 16px;\
    line-height: 24px;\
    border-radius: 4px;\
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;';
    var html = '<button class="woo-btn" style="'+ style +'">\
        <span class="woo-icon">+</span> Woo</botton>';
    return html;
}

Site.prototype.getClose = function() {
    var style = 'opacity: 1; position: absolute; right: 10px; cursor: pointer;';
    var html = '<a id="woo-close" style="'+ style +'">✕</a>';
    return html;
}

Site.prototype.success = function() {
    $("#woo-dropbar").find("button .woo-icon").html("√");
    var msg = "采集成功";
    if(this.isTaobao()) {
        //淘宝的FAQ在独立页面
        msg += ", 继续采集->>" + new Taobao().getFaqLink();
    }
    $("#woo-dropbar").find("#woo-dropmsg").html(msg);
}

//提交中
Site.prototype.processing = function() {
    $("#woo-dropbar").find("button .woo-icon").html("...");
    $("#woo-dropbar").find("#woo-dropmsg").html("正在提交...");
}

Site.prototype.fail = function(msg) {
    $("#woo-dropbar").find("button .woo-icon").html("+");
    $("#woo-dropbar").find("#woo-dropmsg").html(msg || "提交失败, 请重试");
}

Site.prototype.getMessageBox = function() {
    return $("#woo-dropbar").find("#woo-dropmsg");
}

Site.prototype.isTaobao = function() {
    var hosts = [
        "item.taobao.com",
        "detail.tmall.com",
    ]
    var host = location.hostname;
    return host && hosts.indexOf(host) > -1 ? true : false;
}

Site.prototype.isAlibaba = function() {
    var hosts = [
        "detail.1688.com"
    ]
    var host = location.hostname;
    return host && hosts.indexOf(host) > -1 ? true : false;
}

Site.prototype.isTaobaoFaq = function() {
    var hosts = [
        "web.m.taobao.com"
    ]
    var host = location.hostname;
    return host && hosts.indexOf(host) > -1 ? true : false;
}

Site.prototype.isJD = function() {
    var hosts = [
        "item.jd.com"
    ]
    var host = location.hostname;
    return host && hosts.indexOf(host) > -1 ? true : false;
}

Site.prototype.isAliexpress = function() {
    var host = location.hostname;
    return host && host.indexOf(".aliexpress.") > -1 ? true : false;
}

Site.prototype.isAmazon = function() {
    var host = location.hostname,
        pathname = location.pathname;
    var isItemPage = pathname.indexOf('dp') > -1 || pathname.indexOf('product') > -1;
    return host && host.indexOf(".amazon.") > -1 & isItemPage ? true : false;
}

Site.prototype.isYiwugo = function() {
    var hosts = [
        "www.yiwugo.com"
    ]
    var host = location.hostname;
    return host && hosts.indexOf(host) > -1 ? true : false;
}

Site.prototype.isGoogle = function() {
    var hosts = [
        "www.google.com",
        "www.google.com.hk"
    ]
    var host = location.hostname;
    return host && hosts.indexOf(host) > -1 ? true : false;
}

module.exports = Site;