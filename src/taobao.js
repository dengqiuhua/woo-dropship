const {getUrlParams, formatImageUrl, extractMoney} = require("./helper");

var Taobao = function() {
};

//淘宝的sku不能一次性自动获得，需人工选择
Taobao.skuList = [];
Taobao.uniqSku = {};
Taobao.$msgbox = null;

//监听事件
Taobao.prototype.onload = function($msgbox) {
    var has_multi_sku = false;
    var options = this.getSkuOptions(),
        attrName = "";
    if(options.length > 0) {
        for(let opt of options) {
            if(opt.options.length > 1) {
                has_multi_sku = true;
                attrName = opt.name;
                break
            }
        }
    }
    //多sku, 初始化选择
    if(has_multi_sku) {
        $("#root").find("div.skuCate .skuItem").on("click", this.onSelectSku);
        if($msgbox) {
            var faq = this.getFaqLink();
            $msgbox.html('检测到多个[sku], <small>请点击['+ attrName +']等选项</small>.'+ faq);
        }
    }
    Taobao.$msgbox = $msgbox;
}

//选择sku选项
Taobao.prototype.onSelectSku = function() {
    var taobao = new Taobao();
    var sku = taobao.getCurrentSku();
    if(!sku) {
        return;
    }
    if(Taobao.uniqSku[sku.sku]) {
        return
    }
    if(!Taobao.skuList) {
        Taobao.skuList = [];
    }
    Taobao.skuList && Taobao.skuList.push(sku);
    Taobao.uniqSku[sku.sku] = 1;
    //提示
    var faq = taobao.getFaqLink();
    var tip = '已选择 <span style="color: #ffaa22;">'+ Taobao.skuList.length +'</span> 个sku, 待提交. '+ faq;
    Taobao.$msgbox && Taobao.$msgbox.html(tip);
}

//获取产品信息
Taobao.prototype.getProductInfo = function() {
    var product = {};
    product.product_id = this.getProductId();
    product.title = this.getTitle();
    product.price = this.getPrice();
    product.regular_price = product.price;
    product.currency = "CNY";
    product.gallary = this.getGallary();
    product.image = product.gallary.length > 0 ? product.gallary[0] : "";
    product.video = this.getVideo();
    product.attributes = this.getAttributes();
    product.brand = this.filterBrand(product.attributes);
    product.category = "";
    product.desc_images = this.getDescImages();
    product.description = this.getDescription();
    product.options = this.getSkuOptions();
    product.referer = this.getReferer();
    product.source = this.getDomain();
    product.language = this.getLanguage();
    product.store_location = this.getLocation();
    product.faq_list = [];
    product.sales = 0;

    var sku = this.getCurrentSku();
    if(Taobao.skuList.length === 0 && sku) {
        Taobao.skuList.push(sku);
        Taobao.uniqSku[sku.sku] = 1;
    }
    product.sku_list = Taobao.skuList;
    return product;
}

Taobao.prototype.getReferer = function() {
    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
    url += "?id=" + getUrlParams("id");
    var sku = this.getSku();
    if(sku) {
        url += "&skuId="+ sku;
    }
    return url;
}

Taobao.prototype.getDomain = function() {
    var host = document.location.hostname;
    var block = host.split(".");
    if(block.length > 2) {
        return block[block.length - 2] + "." + block[block.length - 1];
    }
    return host;
}

Taobao.prototype.getLanguage = function() {
    return 'zh';
}

Taobao.prototype.getProductId = function() {
    return getUrlParams("id");
}

Taobao.prototype.getTitle = function() {
    var el = $("#root").find("h1");
    if(el.length === 0 ) {
        return document.title;
    }
    return el.first().text().trim();
}

Taobao.prototype.getPrice = function() {
    var el = $("#root").find("span[class^='Price--priceText--']");
    if(el.length === 0 ) {
        el = $("#root").find("span[class^='SecurityPrice--priceText--']");
        if(el.length === 0) {
            return "";
        }
    }
    return extractMoney(el.first().text());
}

Taobao.prototype.getSku = function() {
    return getUrlParams("skuId");
}

//多个sku
Taobao.prototype.getSkuOptions = function() {
    var el = $("#root").find("div.skuCate");
    if(el.length === 0 ) {
        el = $("#root").find("div[class^='SkuContent--skuItem']");
        if(el.length === 0 ) {
            return [];
        }
    }
    var results = [];
    for(var i=0; i < el.length; i++) {
        var items = $(el[i]).find(".skuItem");
        if(items.length === 0) {
            items = $(el[i]).find("div[class^='SkuContent--valueItem']");
            if(items.length === 0) {
                continue;
            }
        }
        var options = [];
        items.each(function(i, n) {
            options.push(n.innerText.trim());
        })
        var name = $(el[i]).find(".skuCateText").text().replace("：", "").trimEnd(":", "");
        if(!name) {
            name = $(el[i]).find("span[class^='ItemLabel--labelText']").text().replace("：", "").trimEnd(":", "");
        }
        if(!name || options.length === 0) {
            continue;
        }
        results.push({name: name, options: options});
    }
    return results;
}

Taobao.prototype.getCurrentSku = function() {
    var el = $("#root").find("div.skuCate");
    var sku = this.getSku();
    if(el.length === 0 || !sku) {
        return null;
    }

    var options = [];
    for(var i=0; i < el.length; i++) {
        var items = $(el[i]).find(".skuItem");
        if(items.length === 0) {
            continue;
        }
        var option = null;
        items.each(function(i, n) {
            if($(n).hasClass("current")) {
                option = n.innerText.trim();
            }
        })
        if(!option) {
            continue
        }
        var name = $(el[i]).find(".skuCateText").text().replace("：", "").trimEnd(":", "");
        options.push({name: name, option: option});
    }
    if(options.length == 0) {
        return null
    }
    let price = this.getPrice(),
        image = this.getCurrentImage();
    return {sku: sku, price: price, image: image, options: options};
}

Taobao.prototype.getGallary = function() {
    var el = $("#root").find("ul[class^='PicGallery--thumbnails--']");
    if(el.length === 0 ) {
        return [];
    }
    var imgs = el.find("img");
    if(imgs.length === 0) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = _this.formatImage(item.src);
        if(urls.indexOf(link) === -1) {
            urls.push(link);
        }
    })
    return urls;
}

Taobao.prototype.getCurrentImage = function() {
    var el = $("#root").find("img[class^='PicGallery--mainPic--']");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Taobao.prototype.getVideo = function() {
    var el = $("#root").find("video.lib-video");
    if(el.length === 0 ) {
        return "";
    }
    var link = el.attr("src");
    return this.formatImage(link);
}

Taobao.prototype.getAttributes = function() {
    var el = $("#root").find("div[class^='ItemDetail--attrs']");
    if(el.length === 0 ) {
        el = $("#root").find("div[class^='DetailInfo']");
        if(el.length === 0 ) {
            return [];
        }
    }
    var attrs = el.find("span[class^='Attrs--attr']");
    if(attrs.length === 0) {
        attrs = el.find("div[class^='InfoItem--infoItem']");
        if(attrs.length === 0) {
            return [];
        }
    }
    var data = [],
        kv = null,
        el_k = null,
        el_v = null;
    attrs.each(function(i, item) {
        el_k = $(this).find("div[class^='InfoItem--infoItemTitle']");
        el_v = $(this).find("div[class^='InfoItem--infoItemContent']");
        if(el_k.length > 0 ** el_v.length > 0) {
            data.push({key: el_k.text().trim(), value: el_v.text().trim()});
        } else {
            kv = item.innerText.replace("：", ":").split(":");
            data.push({key: kv[0], value: kv[1]});
        }
    })
    return data;
}

Taobao.prototype.filterBrand = function(attributes) {
    if(!attributes || attributes.length === 0 ) {
        return "";
    }
    for(var i=0; i < attributes.length; i++) {
        if(attributes[i].key === "品牌") {
            return attributes[i].value;
        }
    }
    return "";
}

Taobao.prototype.getDescImages = function() {
    var el = $("#root").find("div.desc-root");
    if(el.length === 0 ) {
        return [];
    }
    var imgs = el.find("img[data-name='singleImage']");
    if(imgs.length === 0) {
        return [];
    }
    var urls = [],
        link = "",
        _this = this;
    imgs.each(function(i, item) {
        link = $(item).attr("data-src");
        if(!link) {
            link = $(item).attr("src");
            if(!link) {
                return true;
            }
        }
        link = _this.formatImage(link);
        urls.push(link)
    })
    return urls;
}

Taobao.prototype.getDescription = function() {
    var el = $("#root").find("div.desc-root");
    if(el.length === 0 ) {
        return "";
    }
    var text = el.text();
    if(!text) {
        return "";
    }
    var results = [];
    var blocks = text.split("\n");
    for(var i=0; i<blocks.length; i++) {
        blocks[i] = blocks[i].trim();
        if(!blocks[i]) {
            continue;
        }
        results.push(blocks[i]);
    }
    if(results.length === 0) {
        return "";
    }
    text = results.join("\n");
    return text.length > 20 ? text : "";
}

//发货地
Taobao.prototype.getLocation = function() {
    var el = $("#root").find("div[class*='mobile--seller-area']");
    if(el.length === 0 ) {
        el = $("#root").find("div[class*='delivery-from-addr']");
        if(el.length === 0 ) {
            return "";
        }
    }
    let loc = el.text();
    if(loc.endsWith("至")) {
        return loc.substring(0, loc.length - 1);
    }
    return loc;
}

//FAQ页面
Taobao.prototype.getFaqPage = function() {
    return "https://web.m.taobao.com/app/mtb/ask-everyone/list?pha=true&disableNav=YES&refId="+ this.getProductId();
}

//FAQ链接btn
Taobao.prototype.getFaqLink = function() {
    return '<a target="_blank" href="'+ this.getFaqPage() +'">FAQ</a>';
}

//格式化图片地址
Taobao.prototype.formatImage = function(link) {
    if(!link) {
        return "";
    }
    if(link.indexOf(".jpg") > -1 && link.lastIndexOf(".webp") > -1) {
        link = link.substring(0, link.indexOf(".jpg") + 4);
    }
    return formatImageUrl(link);
}

module.exports = Taobao;